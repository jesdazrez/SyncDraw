#include "appargs.h"

#include <limits>

AppArgs::AppArgs(const QStringList &args)
{
    auto len = args.length();
    if (len == 2)
    {
        setUpPort(args[1]);
    }
    else if (len == 3)
    {
        m_hostname = QHostAddress(args[1]);
        setUpPort(args[2]);
    }
    else if (len != 1)
    {
        qFatal("Usage: SyncDraw <hostname>? <port>");
    }
}

const QHostAddress &AppArgs::hostname() const
{
    return m_hostname;
}

unsigned short AppArgs::port() const
{
    return m_port;
}

void AppArgs::setUpPort(const QString &arg)
{
    auto isValid = false;
    auto port = QString(arg).toUInt(&isValid);
    if (!isValid || port > std::numeric_limits<unsigned short>::max())
    {
        qFatal("Invalid port number!");
    }
    m_port = port;
}
