#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "canvas.h"
#include "syncinterface.h"

#include <QMainWindow>
#include <QPen>
#include <QResizeEvent>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(SyncInterface *interface, QWidget *parent = nullptr);
    ~MainWindow();

    const Canvas *canvas() const;
signals:
    void penSelected(QPen pen);
    void resized(int newWidth, int newHeight);
public slots:
    void resizeAtCmd(int newWidth, int newHeight);
    void updatePeersCount(int peersCount);
protected:
    void resizeEvent(QResizeEvent *event) override;
private:
    void activateDrawMode();
    void activateEraseMode();

    Ui::MainWindow *ui;
    SyncInterface *syncInterface;
};
#endif // MAINWINDOW_H
