#ifndef APPARGS_H
#define APPARGS_H

#include <QHostAddress>
#include <QStringList>

class AppArgs
{
public:
    AppArgs(const QStringList &args);
    const QHostAddress &hostname() const;
    unsigned short port() const;
private:
    void setUpPort(const QString &arg);

    QHostAddress m_hostname = QHostAddress("127.0.0.1");
    unsigned short m_port = 57135;
};

#endif // APPARGS_H
