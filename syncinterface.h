#ifndef SERVER_H
#define SERVER_H

#include "flatbuffers/flatbuffers.h"
#include "protocol/interface_generated.h"

#include <QByteArray>
#include <QHostAddress>
#include <QPen>
#include <QPoint>
#include <QProcess>
#include <QString>
#include <QTcpSocket>

class SyncInterface : public QObject
{
    Q_OBJECT

public:
    SyncInterface(QHostAddress hostname, unsigned short port, QString serverExecPath);
    ~SyncInterface();

    bool connectToServer(int timeoutMSecs);
signals:
    void greeted(QString assignedName);
    void networkSizeUpdated(int newSize);
    void lineDrawed(QPoint from, QPoint to, QPen withPen);
    void pointDrawed(QPoint at, QPen withPen);
    void resized(int newWidth, int newHeight);
public slots:
    void drawLine(QPoint from, QPoint to, QPen withPen);
    void drawPoint(QPoint at, QPen withPen);
    void resize(int newWidth, int newHeight);
private slots:
    void launchServer();
    void readFromSocket();
private:
    void retryServerConnection();
    void tryInitNextFrameFromStream();
    bool sendToSocket(flatbuffers::Offset<Message> message);

    Color createColor(const QColor &color) const;
    QColor createNativeColor(const Color &color) const;
    Coord createCoord(const QPoint &point) const;
    QPoint createNativePoint(const Coord *coord) const;
    Pen createPen(const QPen &pen) const;
    QPen createNativePen(const Pen *pen) const;

    void processFrame();
    void processGreetingFrame(const Hello *payload);
    void processNetworkSizeUpdatedFrame(const UpdateNetSizeCmd *payload);
    void processLineDrawedFrame(const DrawLineCmd *payload);
    void processPointDrawedFrame(const DrawPointCmd *payload);
    void processResizedFrame(const ResizeCmd *payload);

    QHostAddress hostname;
    unsigned short port;
    QString serverExecPath;
    QProcess *serverProcess;
    QTcpSocket *socket;

    unsigned int currFrameSize;
    QByteArray buffer;
    flatbuffers::FlatBufferBuilder messageBuilder;

    static const int CONN_ATTEMPT_INTERVAL = 100;
    static const int FRAME_PREFIX_SIZE = sizeof(currFrameSize);
};

#endif // SERVER_H
