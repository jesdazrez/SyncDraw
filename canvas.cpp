#include "canvas.h"

#include <QPainter>
#include <QRect>

Canvas::Canvas(QWidget *parent)
    : QWidget(parent)
    , canvasColor(Qt::white)
    , drawing(false)
{
    // Sketch is static at the top left, optimizes paint event on resizes
    setAttribute(Qt::WA_StaticContents);
    /*
     * The initial blank sketch is created on the resize event when
     * the widget first appears on the screen.
    */
}

void Canvas::setPen(QPen pen)
{
    fitPenToStyle(pen);
    canvasPen = pen;
}

void Canvas::drawLine(QPoint from, QPoint to, QPen withPen)
{
    QPainter painter(&sketch);
    fitPenToStyle(withPen);
    painter.setPen(withPen);
    painter.drawLine(from, to);

    updateModifiedRegion(from, to, withPen.width());
}

void Canvas::drawPointAt(const QPoint &coord, QPen withPen)
{
    QPainter painter(&sketch);
    fitPenToStyle(withPen);
    painter.setPen(withPen);
    painter.drawPoint(coord);

    updateModifiedRegion(coord, coord, withPen.width());
}

void Canvas::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
    {
        drawing = true;
        lastCoord = event->pos();
    }
}

void Canvas::mouseMoveEvent(QMouseEvent *event)
{
    if (drawing)
    {
        drawLineTo(event->pos());
    }
}

void Canvas::mouseReleaseEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton && drawing)
    {
        drawPoint(event->pos());
        drawing = false;
    }
}

void Canvas::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    auto updatedRegion = event->rect();
    painter.drawImage(updatedRegion, sketch, updatedRegion);
}

void Canvas::resizeEvent(QResizeEvent *event)
{
    auto oldSize = event->oldSize();
    if (width() > oldSize.width() || height() > oldSize.height())
    {
        resizeSketch(QSize(width(), height()));
        update();
    }

    QWidget::resizeEvent(event);
}

void Canvas::drawLineTo(const QPoint &coord)
{
    QPainter painter(&sketch);
    painter.setPen(canvasPen);
    painter.drawLine(lastCoord, coord);

    updateModifiedRegion(lastCoord, coord, canvasPen.width());
    emit lineDrawed(lastCoord, coord, canvasPen);

    lastCoord = coord;
}

void Canvas::drawPoint(const QPoint &at)
{
    QPainter painter(&sketch);
    painter.setPen(canvasPen);
    painter.drawPoint(at);

    updateModifiedRegion(at, at, canvasPen.width());
    emit pointDrawed(at, canvasPen);
}

void Canvas::resizeSketch(const QSize &newSize)
{
    QImage newSketch(newSize, QImage::Format_RGB32);
    newSketch.fill(canvasColor);

    QPainter painter(&newSketch);
    painter.drawImage(QPoint(0, 0), sketch);

    sketch = newSketch;
}

void Canvas::updateModifiedRegion(const QPoint &from, const QPoint &to, int penWidth)
{
    auto r = (penWidth / 2) + 2;
    update(QRect(from, to)
           .normalized()
           .adjusted(-r, -r, r, r)
    );
}

void Canvas::fitPenToStyle(QPen &pen)
{
    pen.setStyle(PEN_STYLE);
    pen.setCapStyle(PEN_CAP_STYLE);
    pen.setJoinStyle(PEN_JOIN_STYLE);
}
