#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QActionGroup>
#include <QLabel>

MainWindow::MainWindow(SyncInterface *interface, QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , syncInterface(interface)
{
    ui->setupUi(this);

    // ToolBar setup
    auto modeLabel = new QLabel("<strong>Mode:<strong>", ui->toolBar);
    auto actionGroup = new QActionGroup(ui->toolBar);
    auto centralWidgetMargin = ui->centralWidget->layout()->margin();
    ui->actionDraw->setChecked(true);
    actionGroup->addAction(ui->actionDraw);
    actionGroup->addAction(ui->actionErase);
    ui->toolBar->addWidget(modeLabel);
    ui->toolBar->addAction(ui->actionDraw);
    ui->toolBar->addAction(ui->actionErase);
    ui->toolBar->layout()->setMargin(centralWidgetMargin);
    ui->toolBar->layout()->setSpacing(centralWidgetMargin * 2);

    // Draw (default) / Erase selection logic
    connect(ui->actionDraw, &QAction::triggered, this, &MainWindow::activateDrawMode);
    connect(ui->actionErase, &QAction::triggered, this, &MainWindow::activateEraseMode);
    connect(this, &MainWindow::penSelected, ui->canvas, &Canvas::setPen);
    activateDrawMode();
}

MainWindow::~MainWindow()
{
    delete ui;
    delete syncInterface;
}

const Canvas *MainWindow::canvas() const
{
    return ui->canvas;
}

void MainWindow::resizeAtCmd(int newWidth, int newHeight)
{
    /*
     * This will block the 'resized' signal within the 'resizeEvent'
     * to avoid entering an infinit resize-loop.
     * The blocking goes off when the blocker object goes out of scope.
    */
    const QSignalBlocker blocker(this);
    resize(newWidth, newHeight);
}

void MainWindow::updatePeersCount(int peersCount)
{
    auto msg = peersCount == 1 ? "" : QString::number(peersCount) + " instances connected";
    ui->statusBar->showMessage(msg);
}

void MainWindow::resizeEvent(QResizeEvent *event)
{
    emit resized(width(), height());
    QWidget::resizeEvent(event);
}

void MainWindow::activateDrawMode()
{
    emit penSelected(QPen(Qt::black, 5.0));
}

void MainWindow::activateEraseMode()
{
    emit penSelected(QPen(Qt::white, 5.0));
}
