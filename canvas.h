#ifndef CANVAS_H
#define CANVAS_H

#include <QColor>
#include <QImage>
#include <QMouseEvent>
#include <QPaintEvent>
#include <QPen>
#include <QPoint>
#include <QResizeEvent>
#include <QSize>
#include <QWidget>

class Canvas : public QWidget
{
    Q_OBJECT
public:
    explicit Canvas(QWidget *parent = nullptr);
signals:
    void lineDrawed(QPoint from, QPoint to, QPen withPen);
    void pointDrawed(QPoint at, QPen withPen);
public slots:
    void setPen(QPen pen);
    void drawLine(QPoint from, QPoint to, QPen withPen);
    void drawPointAt(const QPoint &coord, QPen withPen);
protected:
    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void paintEvent(QPaintEvent *event) override;
    void resizeEvent(QResizeEvent *event) override;
private:
    void drawLineTo(const QPoint &coord);
    void drawPoint(const QPoint &at);
    void resizeSketch(const QSize &newSize);
    void updateModifiedRegion(const QPoint &from, const QPoint &to, int penWidth);
    void fitPenToStyle(QPen &pen);

    QPen canvasPen;
    QColor canvasColor;
    QImage sketch;
    QPoint lastCoord;
    bool drawing;

    static const Qt::PenStyle PEN_STYLE = Qt::SolidLine;
    static const Qt::PenCapStyle PEN_CAP_STYLE = Qt::RoundCap;
    static const Qt::PenJoinStyle PEN_JOIN_STYLE = Qt::RoundJoin;
};

#endif // CANVAS_H
