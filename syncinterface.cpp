#include "syncinterface.h"

#include <QCoreApplication>
#include <QEventLoop>
#include <QTimer>

SyncInterface::SyncInterface(QHostAddress hostname, unsigned short port, QString serverExecPath)
    : hostname(hostname)
    , port(port)
    , serverExecPath(serverExecPath)
    , serverProcess(nullptr)
    , socket(new QTcpSocket())
    , currFrameSize(0)
    , buffer(QByteArray::fromRawData(nullptr, 0))
{
    connect(socket, &QTcpSocket::readyRead, this, &SyncInterface::readFromSocket);
}

SyncInterface::~SyncInterface()
{
    delete socket;
    if (serverProcess != nullptr)
    {
        delete serverProcess;
    }
}

bool SyncInterface::connectToServer(int timeoutMSecs)
{
    QTimer timer;
    QEventLoop eventLoop;
    timer.setSingleShot(true);
    QObject::connect(
        socket, QOverload<QAbstractSocket::SocketError>::of(&QAbstractSocket::error),
        [this](QTcpSocket::SocketError err)
        {
            if (err == QTcpSocket::SocketError::ConnectionRefusedError)
            {
                if (serverProcess == nullptr)
                {
                    launchServer();
                }
                else
                {
                    retryServerConnection();
                }
            }
        }
    );
    QObject::connect(socket, &QTcpSocket::connected, &eventLoop, &QEventLoop::quit);
    QObject::connect(&timer, &QTimer::timeout, &eventLoop, &QEventLoop::quit);

    socket->connectToHost(hostname, port);
    timer.start(timeoutMSecs);
    eventLoop.exec();

    return socket->state() == QTcpSocket::SocketState::ConnectedState;
}

void SyncInterface::drawLine(QPoint from, QPoint to, QPen withPen)
{
    auto fromCoord = createCoord(from);
    auto toCoord = createCoord(to);
    auto pen = createPen(withPen);

    messageBuilder.Reset();
    auto payloadOffset = CreateDrawLineCmd(messageBuilder, &fromCoord, &toCoord, &pen);
    auto msgOffset = CreateMessage(messageBuilder, Payload_DrawLineCmd, payloadOffset.Union());

    if (!sendToSocket(msgOffset))
    {
        qDebug() << "Not all DrawLineCmd bytes were sent!";
    }
}

void SyncInterface::drawPoint(QPoint at, QPen withPen)
{
    auto coord = createCoord(at);
    auto pen = createPen(withPen);

    messageBuilder.Reset();
    auto payloadOffset = CreateDrawPointCmd(messageBuilder, &coord, &pen);
    auto msgOffset = CreateMessage(messageBuilder, Payload_DrawPointCmd, payloadOffset.Union());

    if (!sendToSocket(msgOffset))
    {
        qDebug() << "Not all DrawPointCmd bytes were sent!";
    }
}

void SyncInterface::resize(int newWidth, int newHeight)
{
    messageBuilder.Reset();
    auto payloadOffset = CreateResizeCmd(messageBuilder, newWidth, newHeight);
    auto msgOffset = CreateMessage(messageBuilder, Payload_ResizeCmd, payloadOffset.Union());

    if (!sendToSocket(msgOffset))
    {
        qDebug() << "Not all ResizeCmd bytes were sent!";
    }
}

void SyncInterface::launchServer()
{
    if (serverProcess != nullptr)
    {
        return;
    }

    serverProcess = new QProcess;
    serverProcess->setProgram(serverExecPath);
    serverProcess->setWorkingDirectory(QCoreApplication::applicationDirPath());
    serverProcess->setArguments({"-h", hostname.toString(), "-p", QString::number(port)});

    qDebug() << "Launching server!";
    serverProcess->startDetached();
    retryServerConnection();
}

void SyncInterface::readFromSocket()
{
    buffer.append(socket->readAll());

    if (currFrameSize == 0)
    {
        tryInitNextFrameFromStream();
    }

    while (currFrameSize != 0 && static_cast<unsigned int>(buffer.size()) >= currFrameSize)
    {
        processFrame();
    }
}

void SyncInterface::retryServerConnection()
{
    QTimer::singleShot(CONN_ATTEMPT_INTERVAL, [this]()
    {
        qDebug() << "Retrying connection...";
        socket->connectToHost(hostname, port);
    });
}

void SyncInterface::tryInitNextFrameFromStream()
{
    if (buffer.size() < FRAME_PREFIX_SIZE)
    {
        return;
    }

    auto streamPtr = reinterpret_cast<const uint8_t*>(buffer.constData());
    currFrameSize = flatbuffers::GetPrefixedSize(streamPtr) + FRAME_PREFIX_SIZE;
}

bool SyncInterface::sendToSocket(flatbuffers::Offset<Message> message)
{
    messageBuilder.FinishSizePrefixed(message);
    auto bufferPtr = reinterpret_cast<const char *>(messageBuilder.GetBufferPointer());
    auto bytes = QByteArray::fromRawData(bufferPtr, messageBuilder.GetSize());

    return socket->write(bytes) == messageBuilder.GetSize();
}

Color SyncInterface::createColor(const QColor &color) const
{
    return Color(color.red(), color.green(), color.blue(), color.alpha());
}

QColor SyncInterface::createNativeColor(const Color &color) const
{
    return QColor(color.r(), color.g(), color.b(), color.a());
}

Coord SyncInterface::createCoord(const QPoint &point) const
{
    return Coord(point.x(), point.y());
}

QPoint SyncInterface::createNativePoint(const Coord *coord) const
{
    return QPoint(coord->x(), coord->y());
}

Pen SyncInterface::createPen(const QPen &pen) const
{
    return Pen(createColor(pen.color()), pen.width());
}

QPen SyncInterface::createNativePen(const Pen *pen) const
{
    return QPen(createNativeColor(pen->color()), pen->width());
}

void SyncInterface::processFrame()
{
    auto frameBytes = buffer.left(currFrameSize);
    auto message = GetSizePrefixedMessage(frameBytes);
    switch (message->payload_type())
    {
        case Payload_Hello:
            processGreetingFrame(message->payload_as_Hello());
            break;
        case Payload_UpdateNetSizeCmd:
            processNetworkSizeUpdatedFrame(message->payload_as_UpdateNetSizeCmd());
            break;
        case Payload_DrawLineCmd:
            processLineDrawedFrame(message->payload_as_DrawLineCmd());
            break;
        case Payload_DrawPointCmd:
            processPointDrawedFrame(message->payload_as_DrawPointCmd());
            break;
        case Payload_ResizeCmd:
            processResizedFrame(message->payload_as_ResizeCmd());
            break;
        default:
            break;
    }

    buffer.remove(0, currFrameSize);
    currFrameSize = 0;
    tryInitNextFrameFromStream();
}

void SyncInterface::processGreetingFrame(const Hello *payload)
{
    emit greeted("SyncDraw " + QString::fromStdString(payload->name()->str()));

    if (payload->width() != 0 || payload->height() != 0)
    {
        emit resized(payload->width(), payload->height());
    }


    emit networkSizeUpdated(payload->net_size());
}

void SyncInterface::processNetworkSizeUpdatedFrame(const UpdateNetSizeCmd *payload)
{
    emit networkSizeUpdated(payload->size());
}

void SyncInterface::processLineDrawedFrame(const DrawLineCmd *payload)
{
    auto from = createNativePoint(payload->from());
    auto to = createNativePoint(payload->to());
    auto pen = createNativePen(payload->pen());

    emit lineDrawed(from, to, pen);
}

void SyncInterface::processPointDrawedFrame(const DrawPointCmd *payload)
{
    auto at = createNativePoint(payload->at());
    auto pen = createNativePen(payload->pen());

    emit pointDrawed(at, pen);
}

void SyncInterface::processResizedFrame(const ResizeCmd *payload)
{
    emit resized(payload->width(), payload->height());
}
