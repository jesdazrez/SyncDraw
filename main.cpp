#include "mainwindow.h"

#include "appargs.h"

#include <QApplication>
#include <QMessageBox>
#include <QObject>
#include <QString>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    AppArgs args(a.arguments());

    auto syncInterface = new SyncInterface(
        args.hostname(),
        args.port(),
#ifdef Q_OS_WIN
        "sync-draw-server.exe"
#else
        "./sync-draw-server"
#endif
    );

    if (!syncInterface->connectToServer(10000))
    {
        QMessageBox::critical(nullptr, "Network problem",
            "Ooops, this is embarrassing :(\n"
            "There seems to be a network related problem.\n"
            "Please try again later..."
        );
        return 1;
    }

    MainWindow w(syncInterface);

    // Greeting Handler
    QObject::connect(syncInterface, &SyncInterface::greeted, [&](QString assignedName)
    {
        w.setWindowTitle(assignedName);
        QObject::connect(&w, &MainWindow::resized, syncInterface, &SyncInterface::resize);
    });

    // Number Of Instances Synchronization
    QObject::connect(syncInterface, &SyncInterface::networkSizeUpdated, &w, &MainWindow::updatePeersCount);

    // (Draw / Erase) Synchronization
    QObject::connect(syncInterface, &SyncInterface::lineDrawed, w.canvas(), &Canvas::drawLine);
    QObject::connect(syncInterface, &SyncInterface::pointDrawed, w.canvas(), &Canvas::drawPointAt);
    QObject::connect(w.canvas(), &Canvas::lineDrawed, syncInterface, &SyncInterface::drawLine);
    QObject::connect(w.canvas(), &Canvas::pointDrawed, syncInterface, &SyncInterface::drawPoint);

    // Resize Synchronization
    QObject::connect(syncInterface, &SyncInterface::resized, &w, &MainWindow::resizeAtCmd);

    w.show();
    return a.exec();
}
